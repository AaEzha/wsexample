package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	ws "gitlab.com/birowo/wsserver/lib"
)

type Room struct {
	name string
	conn net.Conn
}

func (r Room) CloseConn() {
	mu.Lock()
	conns := rooms[r.name]
	l := len(conns)
	if l != 0 {
		i := 0
		for i < l && conns[i] != r.conn {
			i++
		}
		if i != l {
			conns[i] = conns[l-1]
			conns = conns[:l-1]
			if l != 1 {
				rooms[r.name] = conns
			} else {
				delete(rooms, r.name)
			}
		}
		r.conn.Close()
		fmt.Println("close")
	}
	mu.Unlock()
}

var (
	rooms = map[string][]net.Conn{}
	mu    sync.Mutex
)

func wsServer(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	roomName, ok1 := query["room"]
	user, ok2 := query["user"]
	if !ok1 || roomName[0] == "" || !ok2 || user[0] == "" {
		return
	}
	conn, err := ws.Upgrade(w, r)
	if err != nil {
		fmt.Println("0", err)
		return
	}
	mu.Lock()
	rooms[roomName[0]] = append(rooms[roomName[0]], conn)
	mu.Unlock()
	fmt.Println("open")
	go func(roomName, user string, timer *time.Timer, conn net.Conn) {
		defer Room{roomName, conn}.CloseConn()
		for {
			header, err := ws.ReadHeader(conn)
			if err != nil {
				fmt.Println("1", conn, err, header)
				return
			}
			if header.OpCode == ws.OpClose {
				fmt.Println("OpClose")
				return
			}
			//Reset the Masked flag, server frames must not be masked as RFC6455 says
			header.Mask = ws.UnMask
			payload := make([]byte, header.Length)
			_, err = conn.Read(payload)
			if err != nil {
				fmt.Println("2", err, payload)
			}
			ws.Cipher(payload, header.MaskKey) //unmask client data that is always masked
			payload = []byte(`["` + user + `","` + string(payload) + `"]`)
			header.Length = int64(len(payload))
			//fmt.Println(rooms[_conn.Room])
			if !timer.Stop() {
				<-timer.C
			}
			timer.Reset(20 * time.Second)
			mu.Lock()
			for _, conn := range rooms[roomName] {
				mu.Unlock()
				if err := ws.WriteHeader(conn, header); err != nil {
					fmt.Println("3", err, header)
				} else if _, err := conn.Write(payload); err != nil {
					fmt.Println("4", err, payload)
				}
				mu.Lock()
			}
			mu.Unlock()
		}
	}(roomName[0], user[0], time.AfterFunc(20*time.Second, Room{roomName[0], conn}.CloseConn), conn)
}
func main() {
	port := os.Getenv("PORT")
	if port == "" {
		fmt.Println("$PORT must be set")
		return
	}
	http.Handle("/", http.FileServer(http.Dir("./client")))
	http.HandleFunc("/ws", wsServer)
	http.ListenAndServe(":"+port, nil)
}
